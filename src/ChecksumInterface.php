<?php

namespace Extradevs\OtpspClient;

interface ChecksumInterface
{
    public function calculate(string $secretKey, string $data): string;

    public function verify(string $secretKey, string $data, string $checksum): bool;
}
