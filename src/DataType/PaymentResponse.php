<?php

namespace Extradevs\OtpspClient\DataType;

class PaymentResponse extends ResponseBase
{

    /**
     * @var string
     */
    public $timeout = '';

    /**
     * @var double
     */
    public $total = 0;

    /**
     * @var string
     */
    public $paymentUrl = '';

    /**
     * @var array
     */
    public $errorCodes = [];
}
